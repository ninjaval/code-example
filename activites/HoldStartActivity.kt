package ru.nsu.startup.vibrotinder.activites

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.exceptions.VKApiCodes
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.mvp.holdStart.HoldStartPresenter
import ru.nsu.startup.vibrotinder.mvp.holdStart.HoldStartView
import ru.nsu.startup.vibrotinder.utils.AlertDialogsUtil
import ru.nsu.startup.vibrotinder.utils.Animation
import ru.nsu.startup.vibrotinder.utils.RouterUtil

class HoldStartActivity : BaseActivity(), HoldStartView, VKAuthCallback {

    @InjectPresenter
    lateinit var presenter: HoldStartPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            presenter.coldStart()
        }
        setContentView(R.layout.activity_hold_start)
    }

    override fun navigateToMainScreen() {
        RouterUtil.startActivityNoHistory(this, MainActivity::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!isVkAuthorize(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onLogin(token: VKAccessToken) {
        token.save(ProjectApplication.getPreferences())
        RouterUtil.startActivity(this, RegistrationActivity::class.java, Animation.SLIDE_IN)
    }

    override fun onLoginFailed(errorCode: Int) {
        when (errorCode) {
            VKApiCodes.CODE_UNKNOWN_ERROR -> logger.debug("User has canceled registration")
            else -> showError(errorCode.toString())
        }
    }

    override fun showError(message: String) {
        AlertDialogsUtil.showOneBtnDialog(this, R.string.error_title, message)
    }

    private fun isVkAuthorize(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ): Boolean {
        return VK.onActivityResult(requestCode, resultCode, data, this)
    }
}