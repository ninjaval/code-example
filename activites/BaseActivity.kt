package ru.nsu.startup.vibrotinder.activites

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import org.slf4j.LoggerFactory
import ru.nsu.startup.vibrotinder.utils.Themes
import ru.nsu.startup.vibrotinder.utils.WindowUtil

abstract class BaseActivity : MvpAppCompatActivity() {

    val logger = LoggerFactory.getLogger(this.javaClass.simpleName)

    override fun onCreate(savedInstanceState: Bundle?) {
        logger.info("onCreate")
        setTheme(Themes.getCurrentThemeRes())
        WindowUtil.enableUserInterface(this)
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        logger.info("onStart")
        super.onStart()
    }

    override fun onPause() {
        logger.info("onPause")
        super.onPause()
    }

    override fun onStop() {
        logger.info("onStop")
        super.onStop()
    }

    override fun onDestroy() {
        logger.info("onDestroy")
        super.onDestroy()
    }

    override fun onResume() {
        logger.info("onResume")
        super.onResume()
    }
}