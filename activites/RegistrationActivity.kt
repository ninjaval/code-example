package ru.nsu.startup.vibrotinder.activites

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.arellomobile.mvp.presenter.InjectPresenter
import io.reactivex.Single
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.databinding.ActivityRegistrationBinding
import ru.nsu.startup.vibrotinder.model.User
import ru.nsu.startup.vibrotinder.mvp.registration.RegistrationPresenter
import ru.nsu.startup.vibrotinder.mvp.registration.RegistrationView
import ru.nsu.startup.vibrotinder.utils.AlertDialogsUtil

class RegistrationActivity : BaseToolbarActivity(), RegistrationView {

    @InjectPresenter
    lateinit var presenter: RegistrationPresenter
    private lateinit var binding: ActivityRegistrationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil
            .inflate(layoutInflater, R.layout.activity_registration, null, false)

        setToolbar(binding.toolbar)
        setContentView(binding.root)
    }

    fun getVkUserSingle(): Single<User?> {
        return presenter.getUserSingle()
    }

    override fun showError(message: String) {
        AlertDialogsUtil.showOneBtnDialog(this, R.string.error_title, message)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}