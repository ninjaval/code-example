package ru.nsu.startup.vibrotinder.activites

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.databinding.ActivityAuthBinding
import ru.nsu.startup.vibrotinder.mvp.auth.AuthPresenter
import ru.nsu.startup.vibrotinder.mvp.auth.AuthView

class AuthActivity : BaseToolbarActivity(), AuthView {

    @InjectPresenter
    lateinit var presenter: AuthPresenter
    lateinit var binding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_auth, null, false)
        setToolbar(binding.toolbar)
        setContentView(binding.root)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}