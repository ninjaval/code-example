package ru.nsu.startup.vibrotinder.activites

import android.view.Gravity
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.ViewSwitcher
import androidx.appcompat.view.ContextThemeWrapper
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.databinding.ToolbarBinding
import ru.nsu.startup.vibrotinder.utils.ResUtils

abstract class BaseToolbarActivity : BaseActivity(), ViewSwitcher.ViewFactory {

    private var toolbar: ToolbarBinding? = null

    var toolbarTitle = ResUtils.getStringById(R.string.app_title)
        set(title) {
            toolbar?.toolBarTitle?.setText(title)
            field = title
        }

    var isBackButtonEnable = true
        set(isEnable) {
            when (isEnable) {
                true -> {
                    toolbar?.back?.visibility = View.VISIBLE
                    toolbar?.back?.setOnClickListener { this.onBackPressed() }
                }
                false -> {
                    toolbar?.back?.visibility = View.GONE
                    toolbar?.back?.setOnClickListener(null)
                }
            }
            field = isEnable
        }

    override fun makeView(): View {
        val textView = TextView(
            ContextThemeWrapper(this, R.style.TitleStyle)
        )
        textView.gravity = Gravity.CENTER
        return textView
    }

    fun setToolbar(
        toolbar: ToolbarBinding,
        toolbarTitle: String = ResUtils.getStringById(R.string.app_title),
        isBackButtonEnable: Boolean = true
    ) {
        initTextSwitcher(toolbar)
        this.toolbar = toolbar
        this.toolbarTitle = toolbarTitle
        this.isBackButtonEnable = isBackButtonEnable
    }

    private fun initTextSwitcher(toolbar: ToolbarBinding) {
        toolbar.toolBarTitle.setFactory(this)
        toolbar.toolBarTitle.inAnimation = AnimationUtils
            .loadAnimation(this, R.anim.title_in)
        toolbar.toolBarTitle.outAnimation = AnimationUtils
            .loadAnimation(this, R.anim.title_out)
    }
}