package ru.nsu.startup.vibrotinder.activites

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.databinding.ActivityEditProfileBinding

class EditProfileActivity : BaseToolbarActivity() {

    lateinit var binding: ActivityEditProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil
            .inflate(layoutInflater, R.layout.activity_edit_profile, null, false)
        setToolbar(binding.toolbar)
        setContentView(binding.root)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
}