package ru.nsu.startup.vibrotinder.activites

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.databinding.ActivityMainBinding
import ru.nsu.startup.vibrotinder.mvp.main.MainActivityPresenter
import ru.nsu.startup.vibrotinder.mvp.main.MainActivityView
import ru.nsu.startup.vibrotinder.mvp.profile.ProfileFragment
import ru.nsu.startup.vibrotinder.utils.KeyStore
import ru.nsu.startup.vibrotinder.utils.WindowUtil

class MainActivity : BaseActivity(), MainActivityView {

    @InjectPresenter
    lateinit var presenter: MainActivityPresenter
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowUtil.enableTransparentStatusBar(this)
        binding = DataBindingUtil
            .inflate(layoutInflater, R.layout.activity_main, null, false)
        setContentView(binding.root)
        setSignedIn()
        initUI()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            supportFragmentManager
                .fragments
                .filterIsInstance<ProfileFragment>()
                .first()
                .presenter
                .loadFullUserFromDatabase()
        }
    }

    override fun onDestroy() {
        WindowUtil.disableTransparentStatusBar(this)
        super.onDestroy()
    }

    private fun initUI() {
        binding.bottomNavigationView.setupWithNavController(
            Navigation.findNavController(this, R.id.main_nav_host_fragment)
        )
    }

    private fun setSignedIn() {
        ProjectApplication
            .getPreferences()
            .edit()
            .putBoolean(KeyStore.KEY_IS_SIGNED_IN, true)
            .apply()
    }
}
