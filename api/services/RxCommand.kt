package ru.nsu.startup.vibrotinder.api.services

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse

class RxCommand {

    companion object {

        fun <R, T : BaseJsonResponse> getApiSingle(
            request: R,
            function: (ApiRequester, R) -> T
        ): Single<T> {
            return Single.fromCallable {
                function.invoke(
                    ApiRequester,
                    request
                )
            }
        }

        fun <T : BaseJsonResponse> getApiSubscribe(
            single: Single<T>,
            onComplete: (() -> Unit)? = null,
            onSuccess: ((T) -> Unit)? = null,
            onError: ((ApiError) -> Unit)? = null
        ): Disposable {
            return single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        onComplete?.invoke()
                        onSuccess?.invoke(it)
                    },
                    { error ->
                        val apiError = when (error) {
                            is ApiError -> error
                            else -> {
                                error.printStackTrace()
                                val response = BaseJsonResponse(ApiError(error.localizedMessage, 0))
                                ApiError(response)
                            }
                        }
                        onComplete?.invoke()
                        onError?.invoke(apiError)
                    }
                )
        }

        fun <R, T : BaseJsonResponse> makeSingleApiRequest(
            request: R,
            function: (ApiRequester, R) -> T,
            onComplete: (() -> Unit)? = null,
            onSuccess: ((T) -> Unit)? = null,
            onError: ((ApiError) -> Unit)? = null
        ): Disposable {
            val single = getApiSingle(request, function)
            return getApiSubscribe(single, onComplete, onSuccess, onError)
        }
    }
}