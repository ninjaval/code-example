package ru.nsu.startup.vibrotinder.api.services

import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse

class ApiError(
    val msg: String,
    private val code: Int = 0
) : Error(msg) {

    constructor(response: BaseJsonResponse) : this(
        response.error.toString(),
        response.error?.code ?: 0
    )
}