package ru.nsu.startup.vibrotinder.api.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestClient(
    baseUrl: String,
    timeout: Long = 10
) : BaseHttpClient<Retrofit>(timeout) {

    override val client: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    override fun <S> createService(cls: Class<S>): S {
        return client.create(cls)
    }
}