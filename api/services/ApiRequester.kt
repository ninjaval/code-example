package ru.nsu.startup.vibrotinder.api.services

import android.util.Base64
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.utils.ResUtils
import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse
import ru.nsu.startup.vibrotinder.api.json.request.IsUserExistsRequest
import ru.nsu.startup.vibrotinder.api.json.response.ObtainTokenJsonResponse
import ru.nsu.startup.vibrotinder.api.json.response.SignUpJsonResponse
import ru.nsu.startup.vibrotinder.api.json.request.ObtainAccessTokenRequest
import ru.nsu.startup.vibrotinder.api.json.request.SignInRequest
import ru.nsu.startup.vibrotinder.api.json.request.SignUpRequest
import ru.nsu.startup.vibrotinder.api.json.response.IsUserExistsResponse
import ru.nsu.startup.vibrotinder.dataBase.DataBaseHelper
import ru.nsu.startup.vibrotinder.model.User
import ru.nsu.startup.vibrotinder.utils.KeyStore
import java.io.IOException
import java.net.SocketTimeoutException

object ApiRequester {

    private var serverApi: ServerApi = RestClient(getBaseUrl())
        .createService(ServerApi::class.java)

    fun checkUserExists(request: IsUserExistsRequest): IsUserExistsResponse {
        val call = serverApi.checkIsUserExist(request.email)
        return makeRequest(call)
    }

    fun signIn(request: SignInRequest): ObtainTokenJsonResponse {
        val call = serverApi.postUserAuthorization(
            getCredentials(),
            request.grantType,
            request.email,
            request.password
        )
        val response = makeRequest(call)
        updateTokenInPreferences(response)
        return response
    }

    fun signUp(request: SignUpRequest): SignUpJsonResponse {
        val call = serverApi.postUserRegistration(request)
        val response = makeRequest(call)
        val user = User.fromSignUpResponse(response)
        DataBaseHelper.getInstance()
            .insert(DataBaseHelper.TABLE_USER, user, User::class)
        return response
    }

    fun obtainAccessToken(request: ObtainAccessTokenRequest): ObtainTokenJsonResponse {
        val call = serverApi.postObtainAccessToken(
            getCredentials(),
            request.grantType,
            request.refreshToken
        )
        val response = makeRequest(call)
        updateTokenInPreferences(response)
        return response
    }

    private fun <T : BaseJsonResponse> makeRequest(
        httpCall: Call<T>,
        responseValidator: ((T) -> Boolean)? = null
    ): T {
        try {
            val response = httpCall.execute()
            checkResponse(response)
            validateResponse(response, responseValidator)
            return response.body() as T
        } catch (e: SocketTimeoutException) {
            e.printStackTrace()
            throw ApiError(e.localizedMessage)
        } catch (e: IOException) {
            e.printStackTrace()
            throw ApiError(e.localizedMessage)
        } catch (e: ApiError) {
            throw e
        }
    }

    private fun updateTokenInPreferences(tokens: ObtainTokenJsonResponse) {
        ProjectApplication.getPreferences()
            .edit()
            .putString(KeyStore.KEY_ACCESS_TOKEN, tokens.accessToken)
            .putString(KeyStore.KEY_REFRESH_TOKEN, tokens.refreshToken)
            .apply()
    }

    @Throws(ApiError::class)
    private fun <T> checkResponse(response: Response<T>) {
        if (!response.isSuccessful) {
            val apiError = prepareError(response)
            val result = BaseJsonResponse(apiError)
            throw ApiError(result)
        }
    }

    @Throws(ApiError::class)
    private fun <T> validateResponse(
        response: Response<T>,
        responseValidator: ((T) -> Boolean)? = null
    ) {
        val body = response.body() ?: return
        responseValidator ?: return
        if (!responseValidator.invoke(body)) {
            throw ApiError(prepareError(response).msg)
        }
    }

    private fun <T> prepareError(response: Response<T>): ApiError {
        val baseJsonResponse = Gson().fromJson(
            response.errorBody()?.string(),
            BaseJsonResponse::class.java
        )
        return baseJsonResponse.error ?: ApiError("Request failed")
    }

    private fun getCredentials(): String {
        val username = ResUtils.getStringById(R.string.credentials_username)
        val password = ResUtils.getStringById(R.string.credentials_password)
        val plainCredsBytes = "$username:$password".toByteArray()
        val encodedString = Base64.encodeToString(plainCredsBytes, Base64.NO_WRAP)
        return "Basic $encodedString"
    }

    private fun getBaseUrl(): String {
        val serverIp = ResUtils.getStringById(R.string.server_ip)
        val serverPort = ResUtils.getStringById(R.string.server_port)
        return "http://$serverIp:$serverPort/"
    }
}