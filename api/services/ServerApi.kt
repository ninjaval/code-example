package ru.nsu.startup.vibrotinder.api.services

import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Header
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Body
import retrofit2.http.Query
import ru.nsu.startup.vibrotinder.api.json.request.SignUpRequest
import ru.nsu.startup.vibrotinder.api.json.response.IsUserExistsResponse
import ru.nsu.startup.vibrotinder.api.json.response.ObtainTokenJsonResponse
import ru.nsu.startup.vibrotinder.api.json.response.SignUpJsonResponse

interface ServerApi {

    companion object {
        private const val JSON_CONTENT_HEADER = "Content-Type: application/json"
    }

    @GET("isUserExists")
    @Headers(JSON_CONTENT_HEADER)
    fun checkIsUserExist(
        @Query("email") email: String
    ): Call<IsUserExistsResponse>

    @POST("oauth/token")
    @FormUrlEncoded
    fun postUserAuthorization(
        @Header("authorization") authorization: String,
        @Field("grant_type") grantType: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ObtainTokenJsonResponse>

    @POST("oauth/token")
    @FormUrlEncoded
    fun postObtainAccessToken(
        @Header("authorization") authorization: String,
        @Field("grant_type") grantType: String,
        @Field("refresh_token") refreshToken: String
    ): Call<ObtainTokenJsonResponse>

    @POST("signup")
    @Headers(JSON_CONTENT_HEADER)
    fun postUserRegistration(
        @Body user: SignUpRequest
    ): Call<SignUpJsonResponse>
}