package ru.nsu.startup.vibrotinder.api.services

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

abstract class BaseHttpClient<T>(timeout: Long) {

    val okHttpClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(timeout, TimeUnit.SECONDS)
        .readTimeout(timeout, TimeUnit.SECONDS)
        .writeTimeout(timeout, TimeUnit.SECONDS)
        .build()

    protected abstract val client: T

    abstract fun <S> createService(cls: Class<S>): S
}
