package ru.nsu.startup.vibrotinder.api.json.request

data class ObtainAccessTokenRequest(
    val refreshToken: String,
    val grantType: String = "refresh_token"
)