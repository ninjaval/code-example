package ru.nsu.startup.vibrotinder.api.json.request

data class IsUserExistsRequest(
    val email: String
)