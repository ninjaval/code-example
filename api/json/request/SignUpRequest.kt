package ru.nsu.startup.vibrotinder.api.json.request

import com.google.gson.annotations.SerializedName

data class SignUpRequest(
    @SerializedName("name")
    val name: String,
    @SerializedName("surname")
    val surname: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("sex")
    val sex: Long,
    @SerializedName("height")
    val height: Long,
    @SerializedName("weight")
    val weight: Long,
    @SerializedName("zone_offset")
    val zoneOffset: String,
    @SerializedName("password")
    val password: String
)