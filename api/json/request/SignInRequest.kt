package ru.nsu.startup.vibrotinder.api.json.request

data class SignInRequest(
    val email: String,
    val password: String,
    val grantType: String = "password"
)