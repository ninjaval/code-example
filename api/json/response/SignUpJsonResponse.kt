package ru.nsu.startup.vibrotinder.api.json.response

import com.google.gson.annotations.SerializedName
import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse

data class SignUpJsonResponse(
    @SerializedName("name")
    val name: String,
    @SerializedName("surname")
    val surname: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("birthday")
    val birthday: String,
    @SerializedName("sex")
    val sex: Long,
    @SerializedName("height")
    val height: Long,
    @SerializedName("weight")
    val weight: Long,
    @SerializedName("zone_offset")
    val zoneOffset: String
) : BaseJsonResponse()