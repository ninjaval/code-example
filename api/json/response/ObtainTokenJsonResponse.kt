package ru.nsu.startup.vibrotinder.api.json.response

import com.google.gson.annotations.SerializedName
import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse

data class ObtainTokenJsonResponse(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("scope")
    val scope: String
) : BaseJsonResponse()