package ru.nsu.startup.vibrotinder.api.json.response

import com.google.gson.annotations.SerializedName
import ru.nsu.startup.vibrotinder.api.json.BaseJsonResponse

data class IsUserExistsResponse(
    @SerializedName("is_exist")
    val isExist: Boolean
) : BaseJsonResponse()