package ru.nsu.startup.vibrotinder.api.json

abstract class BaseResponse {

    private var wasError: Boolean = false

    var errorMessage: String = ""
        set(value) {
            wasError = true
            field = value
        }

    var errorCode: Int = 0
        set(value) {
            wasError = true
            field = value
        }

    fun isFailed(): Boolean = wasError
    fun isSuccess(): Boolean = !wasError
}