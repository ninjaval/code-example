package ru.nsu.startup.vibrotinder.api.json

import com.google.gson.annotations.SerializedName
import ru.nsu.startup.vibrotinder.api.services.ApiError

open class BaseJsonResponse(
    @SerializedName("error")
    var error: ApiError? = null,
    var baseResponse: BaseResponse? = null
) {

    fun isSuccess(): Boolean {
        return error == null
    }

    fun isFailed(): Boolean {
        return !isSuccess()
    }
}