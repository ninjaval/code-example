package ru.nsu.startup.vibrotinder.mvp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import org.slf4j.LoggerFactory
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.activites.BaseToolbarActivity
import ru.nsu.startup.vibrotinder.utils.ResUtils

abstract class BaseFragment : MvpAppCompatFragment() {

    val logger = LoggerFactory.getLogger(this.javaClass.simpleName)

    open var titleRes = R.string.app_title

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logger.info("onCreate")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        logger.info("onActivityCreated")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        logger.info("onCreateView")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logger.info("onViewCreate")

        if (activity is BaseToolbarActivity) {
            (activity as BaseToolbarActivity).toolbarTitle = ResUtils.getStringById(titleRes)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        logger.info("onSavedInstanceState")
    }

    override fun onResume() {
        super.onResume()
        logger.info("onResume")
    }

    override fun onStart() {
        super.onStart()
        logger.info("onStart")
    }

    override fun onStop() {
        super.onStop()
        logger.info("onStop")
    }

    override fun onPause() {
        logger.info("onPause")
        super.onPause()
    }

    override fun onDestroy() {
        logger.info("onDestroy")
        super.onDestroy()
    }

    private fun showToast(text: String) {
        Toast.makeText(
            ProjectApplication.applicationInstance.applicationContext,
            text,
            Toast.LENGTH_SHORT
        ).show()
    }

    fun showToastContentIsNotAvailable() {
        showToast(getString(R.string.content_is_not_available))
    }
}