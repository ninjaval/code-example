package ru.nsu.startup.vibrotinder.mvp.enter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.activites.MainActivity
import ru.nsu.startup.vibrotinder.databinding.FragmentEnterBinding
import ru.nsu.startup.vibrotinder.mvp.BaseValidationFragment
import ru.nsu.startup.vibrotinder.utils.AlertDialogsUtil
import ru.nsu.startup.vibrotinder.utils.RouterUtil
import ru.nsu.startup.vibrotinder.utils.WindowUtil
import ru.nsu.startup.vibrotinder.validators.EmailValidator
import ru.nsu.startup.vibrotinder.validators.PasswordValidator

class EnterFragment : BaseValidationFragment(), EnterView {

    @InjectPresenter
    lateinit var presenter: EnterPresenter
    private lateinit var binding: FragmentEnterBinding

    override var titleRes = R.string.enter_title

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_enter, container, false)
        initUI()
        return binding.root
    }

    override fun navigateToMainScreen() {
        RouterUtil.startActivityNoHistory(activity, MainActivity::class.java)
    }

    private fun initUI() {
        addValidators(
            binding.enter, arrayListOf(
                EmailValidator(binding.email),
                PasswordValidator(binding.password)
            )
        )

        binding.forgotPassword.setOnClickListener {
            showToastContentIsNotAvailable()
            // it.findNavController().navigate(R.id.action_enterFragment_to_forgotPasswordFragment)
        }

        binding.enter.setOnClickListener {
            presenter.signIn(
                binding.email.editText?.text.toString(),
                binding.password.editText?.text.toString()
            )
        }
    }

    override fun showError(message: String) {
        AlertDialogsUtil.showOneBtnDialog(activity, R.string.error_title, message)
    }

    override fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
        WindowUtil.disableUserInterface(activity)
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
        WindowUtil.enableUserInterface(activity)
    }
}
