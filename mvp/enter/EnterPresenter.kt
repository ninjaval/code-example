package ru.nsu.startup.vibrotinder.mvp.enter

import com.arellomobile.mvp.InjectViewState
import ru.nsu.startup.vibrotinder.api.json.request.SignInRequest
import ru.nsu.startup.vibrotinder.api.services.RxCommand
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter

@InjectViewState
class EnterPresenter : BaseMvpPresenter<EnterView>() {

    fun signIn(email: String, password: String) {
        logger.info("signIn: Email $email; Password $password")
        viewState.showProgress()

        regSubs(RxCommand.makeSingleApiRequest(
            request = SignInRequest(email, password),
            function = { apiRequester, signInRequest ->
                apiRequester.signIn(signInRequest)
            },
            onComplete = {
                viewState.hideProgress()
            },
            onSuccess = {
                logger.info("onSuccessSignIn")
                viewState.navigateToMainScreen()
            },
            onError = {
                viewState.showError(it.msg)
            }
        ))
    }
}