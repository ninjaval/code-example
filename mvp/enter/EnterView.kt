package ru.nsu.startup.vibrotinder.mvp.enter

import com.arellomobile.mvp.MvpView

interface EnterView : MvpView {

    fun navigateToMainScreen()

    fun showError(message: String)

    fun showProgress()

    fun hideProgress()
}