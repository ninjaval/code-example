package ru.nsu.startup.vibrotinder.mvp.profile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.activites.EditProfileActivity
import ru.nsu.startup.vibrotinder.activites.HoldStartActivity
import ru.nsu.startup.vibrotinder.databinding.FragmentProfileBinding
import ru.nsu.startup.vibrotinder.model.vk.VkCounters
import ru.nsu.startup.vibrotinder.model.vk.VkPersonal
import ru.nsu.startup.vibrotinder.model.vk.VkUser
import ru.nsu.startup.vibrotinder.mvp.BaseFragment
import ru.nsu.startup.vibrotinder.ui.ProfileFactory
import ru.nsu.startup.vibrotinder.utils.AlertDialogsUtil
import ru.nsu.startup.vibrotinder.utils.Animation
import ru.nsu.startup.vibrotinder.utils.RouterUtil
import ru.nsu.startup.vibrotinder.utils.Sex
import ru.nsu.startup.vibrotinder.utils.Themes
import ru.nsu.startup.vibrotinder.utils.TimeUtil
import ru.nsu.startup.vibrotinder.utils.WindowUtil
import java.io.Serializable

class ProfileFragment : BaseFragment(), ProfileView {

    @InjectPresenter
    lateinit var presenter: ProfilePresenter
    private lateinit var binding: FragmentProfileBinding

    private val requestOptions = RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.ic_profile_avatar_stub)
        .error(R.drawable.ic_profile_avatar_stub)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        presenter.loadFullUserFromDatabase()
        initUI()
        return binding.root
    }

    private fun initUI() {
        RouterUtil.startActivityForResultByClick(
            binding.edit,
            activity,
            EditProfileActivity::class.java,
            Animation.SLIDE_IN
        )

        binding.moreInfoButton.setOnClickListener {
            val intentOpen = binding.moreInfoContainer.visibility == View.GONE
            changeMoreInfoState(intentOpen)
        }

        binding.changeThemeButton.setOnClickListener {
            AlertDialogsUtil.showThemePickerDialog(
                activity,
                this,
                Themes.getCurrentTheme(),
                {
                    Themes.setTheme(it, activity, getExtra())
                }
            )
        }

        binding.syncVkButton.setOnClickListener {
            showVkSyncDialog()
        }

        binding.logoutButton.setOnClickListener {
            AlertDialogsUtil.showTwoBtnDialog(
                activity,
                R.string.profile_exit_title,
                R.string.profile_exit_description,
                R.string.yes,
                R.string.cancel,
                {
                    presenter.logout()
                    RouterUtil.startActivityNoHistory(activity, HoldStartActivity::class.java)
                }
            )
        }
    }

    private fun getExtra(): Map<String, Serializable> {
        val extra = mutableMapOf<String, Serializable>()
        extra[KEY_MORE_INFO_STATE] = binding.moreInfoContainer.visibility == View.VISIBLE
        extra[KEY_COUNTERS_STATE] = binding.horizontalScroll.visibility == View.VISIBLE
        extra[KEY_SCROLL_Y] = binding.mainScroll.scrollY
        extra[KEY_SCROLL_X] = binding.horizontalScroll.scrollX
        return extra
    }

    override fun restoreExtra() {
        val intent = activity?.intent
        intent ?: return

        val isMoreInfoVisible = intent.getSerializableExtra(KEY_MORE_INFO_STATE) as Boolean?
        changeMoreInfoState(isMoreInfoVisible)
        val isCountersVisible = intent.getSerializableExtra(KEY_COUNTERS_STATE) as Boolean?
        changeCountersState(isCountersVisible)

        binding.mainScroll.post {
            val scrollY = intent.getSerializableExtra(KEY_SCROLL_Y) as Int?
            binding.mainScroll.scrollY = scrollY ?: 0
        }
        binding.horizontalScroll.post {
            val scrollX = intent.getSerializableExtra(KEY_SCROLL_X) as Int?
            binding.horizontalScroll.scrollX = scrollX ?: 0
        }
        activity?.intent = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.checkFirstTimeProfile()
    }

    override fun showVkSyncDialog() {
        AlertDialogsUtil.showTwoBtnDialog(
            ctx = activity,
            title = R.string.profile_vk_data_title,
            msg = R.string.profile_vk_data_description,
            positiveCallback = {
                presenter.syncVkUser()
            }
        )
    }

    @SuppressLint("SetTextI18n")
    override fun setUserFields(user: VkUser) {
        Glide.with(binding.avatar)
            .load(user.photoMax)
            .apply(requestOptions)
            .into(binding.avatar)

        binding.nameSurname.text = "${user.firstName} ${user.lastName}"

        cleanProfileFields()
        val profileFields = getProfileFields(user)

        // set <MAIN_FIELDS_COUNT> fields
        val mainFields = profileFields.take(MAIN_FIELDS_COUNT)
        mainFields.forEach {
            binding.infoContainer.addView(it)
        }

        // set other fields under spoiler
        val otherFields = profileFields.drop(MAIN_FIELDS_COUNT)
        if (otherFields.isNotEmpty()) {
            otherFields.forEach {
                binding.moreInfoContainer.addView(it)
            }
            binding.moreInfoBlock.visibility = View.VISIBLE
        }

        // counters horizontal layout
        val countersFields = getCountersFields(user.counters)
        changeCountersState(countersFields.isNotEmpty())
        countersFields.forEach {
            binding.horizontalContainer.addView(it)
        }
    }

    private fun getCountersFields(counters: VkCounters?): List<View> {
        counters ?: return listOf()
        val fields = mutableListOf<View>()
        val factory = ProfileFactory(activity, fields)
        factory.addCountersView(R.string.profile_albums, R.drawable.ic_photo, counters.albums)
        factory.addCountersView(R.string.profile_audios, R.drawable.ic_headphones, counters.audios)
        factory.addCountersView(
            R.string.profile_photos,
            R.drawable.ic_digital_camera,
            counters.photos
        )
        factory.addCountersView(R.string.profile_notes, R.drawable.ic_clipboard, counters.notes)
        factory.addCountersView(
            R.string.profile_friends,
            R.drawable.ic_friendship,
            counters.friends
        )
        factory.addCountersView(R.string.profile_groups, R.drawable.ic_connection, counters.groups)
        factory.addCountersView(R.string.profile_followers, R.drawable.ic_star, counters.followers)
        factory.addCountersView(R.string.profile_pages, R.drawable.ic_cv, counters.interestsPages)
        return fields
    }

    private fun getProfileFields(user: VkUser): List<View> {
        val fields = mutableListOf<View>()
        val factory = ProfileFactory(activity, fields)

        factory.addSimpleView(user.about, R.drawable.ic_bell)
        factory.addSimpleView(user.activities, R.drawable.ic_no_icon)
        factory.addSimpleView(TimeUtil.reformBirthday(user.birthday), R.drawable.ic_cake)
        factory.addSimpleView(user.books, R.drawable.ic_book)

        // TODO("Implement this")
        // val career: VkCareer? = null

        val place = concat(user.country?.title, user.city?.title)
        factory.addSimpleView(place, R.drawable.ic_location_city)

        // TODO("Implement this")
        // val connections: Array<String>? = null,

        // TODO("Implement this")
        // val education: VkEducation? = null,

        factory.addSimpleView(user.games, R.drawable.ic_videogame)

        // TODO("Implement this")
        // val homeTown: String? = null,

        factory.addSimpleView(user.interests, R.drawable.ic_no_icon)
        factory.addSimpleView(user.maidenName, R.drawable.ic_no_icon)
        factory.addSimpleView(user.movies, R.drawable.ic_movie)
        factory.addSimpleView(user.music, R.drawable.ic_music)
        factory.addSimpleView(user.occupation?.name, R.drawable.ic_no_icon)
        val political = VkPersonal.politicalFromPoliticalId(user.personal?.political)
        factory.addSimpleView(political, R.drawable.ic_no_icon)
        factory.addSimpleView(user.personal?.langs?.toLine(), R.drawable.ic_no_icon)
        factory.addSimpleView(user.personal?.ideology, R.drawable.ic_no_icon)
        factory.addSimpleView(user.personal?.sourcesOfInspiration, R.drawable.ic_no_icon)
        val peopleMain = VkPersonal.peopleMainFromPeopleMainId(user.personal?.peopleMain)
        factory.addSimpleView(peopleMain, R.drawable.ic_no_icon)
        val lifeMain = VkPersonal.lifeMainFromLifeMainId(user.personal?.lifeMain)
        factory.addSimpleView(lifeMain, R.drawable.ic_no_icon)
        val smoking = VkPersonal.smokingFromSmokingId(user.personal?.smoking)
        factory.addSimpleView(smoking, R.drawable.ic_no_icon)
        val alcohol = VkPersonal.alcoholFromAlcoholId(user.personal?.alcohol)
        factory.addSimpleView(alcohol, R.drawable.ic_no_icon)
        factory.addSimpleView(user.quotes, R.drawable.ic_no_icon)
        val relation = VkUser.relationFromRelationId(user.relation)
        factory.addSimpleView(relation, R.drawable.ic_no_icon)

        // TODO("Implement this")
        // val schools: Array<VkSchool>? = null,

        factory.addSimpleView(Sex.fromCode(user.sex.toLong()).toString(), R.drawable.ic_no_icon)
        factory.addSimpleView(user.site, R.drawable.ic_no_icon)
        factory.addSimpleView(user.status, R.drawable.ic_chat)

        // TODO("Implement this")
        // val trending: Int? = null,

        factory.addSimpleView(user.tv, R.drawable.ic_no_icon)

        // TODO("Implement this")
        // val universities: Array<VkUniversity>? = null,

        factory.addSimpleView(user.email, R.drawable.ic_email)
        return fields
    }

    private fun cleanProfileFields() {
        binding.infoContainer.removeAllViews()
        binding.moreInfoContainer.removeAllViews()
        binding.horizontalContainer.removeAllViews()
        binding.moreInfoBlock.visibility = View.GONE
        changeMoreInfoState(false)
        changeCountersState(false)
    }

    private fun List<String>?.toLine(): String {
        if (this.isNullOrEmpty()) {
            return ""
        }
        val sb = StringBuilder(this[0])
        for (i in 1 until this.size) {
            sb.append(", ")
            sb.append(this[i])
        }
        return sb.toString()
    }

    private fun concat(title1: String?, title2: String?): String? {
        return when {
            title1.isNullOrEmpty() -> title2
            title2.isNullOrEmpty() -> title1
            else -> "$title1, $title2"
        }
    }

    override fun showError(message: String) {
        hideProgress()
        AlertDialogsUtil.showOneBtnDialog(activity, R.string.error_title, message)
    }

    override fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
        WindowUtil.disableUserInterface(activity)
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
        WindowUtil.enableUserInterface(activity)
    }

    @SuppressLint("CheckResult")
    private fun changeMoreInfoState(isVisible: Boolean?) {
        isVisible ?: return
        if (isVisible) {
            binding.moreInfoContainer.visibility = View.VISIBLE
            binding.moreInfoIcon.setImageResource(R.drawable.ic_arrow_up)
        } else {
            binding.moreInfoContainer.visibility = View.GONE
            binding.moreInfoIcon.setImageResource(R.drawable.ic_arrow_down)
        }
    }

    private fun changeCountersState(isVisible: Boolean?) {
        isVisible ?: return
        if (isVisible) {
            binding.horizontalScroll.visibility = View.VISIBLE
            binding.separator2.visibility = View.VISIBLE
        } else {
            binding.horizontalScroll.visibility = View.GONE
            binding.separator2.visibility = View.GONE
        }
    }

    companion object {
        private const val MAIN_FIELDS_COUNT = 4

        private const val KEY_SCROLL_Y = "KEY_SCROLL_Y"
        private const val KEY_SCROLL_X = "KEY_SCROLL_X"
        private const val KEY_MORE_INFO_STATE = "KEY_MORE_INFO_STATE"
        private const val KEY_COUNTERS_STATE = "KEY_COUNTERS_STATE"
    }
}