package ru.nsu.startup.vibrotinder.mvp.profile

import android.annotation.SuppressLint
import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.dataBase.DataBaseHelper
import ru.nsu.startup.vibrotinder.model.FullUser
import ru.nsu.startup.vibrotinder.model.vk.VkUser
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter
import ru.nsu.startup.vibrotinder.utils.KeyStore
import ru.nsu.startup.vibrotinder.utils.VkStubs
import ru.nsu.startup.vibrotinder.utils.VkUtil

@InjectViewState
class ProfilePresenter : BaseMvpPresenter<ProfileView>() {

    fun checkFirstTimeProfile() {
        if (isFirstTimeProfile()) {
            viewState.showVkSyncDialog()
            setFirstTimeProfile(false)
        }
    }

    private fun isFirstTimeProfile(): Boolean {
        return ProjectApplication
            .getPreferences()
            .getBoolean(KeyStore.KEY_IS_FIRST_TIME_PROFILE, true)
    }

    private fun setFirstTimeProfile(isFirstTime: Boolean) {
        ProjectApplication
            .getPreferences()
            .edit()
            .putBoolean(KeyStore.KEY_IS_FIRST_TIME_PROFILE, isFirstTime)
            .apply()
    }

    @SuppressLint("CheckResult")
    fun syncVkUser() {
        viewState.showProgress()
        val token = VkStubs.restore(ProjectApplication.getPreferences())
        VkUtil.getMe(token, VkUtil.FULL)
            .map { it.response?.get(0) }
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.hideProgress()
                it ?: return@subscribe
                it.email = token?.email
                saveFullUserInDatabase(it)
                viewState.setUserFields(it)
            }, {
                viewState.hideProgress()
                viewState.showError(it.localizedMessage)
            })
    }

    fun logout() {
        ProjectApplication.getPreferences()
            .edit()
            .remove(KeyStore.KEY_IS_SIGNED_IN)
            .remove(KeyStore.KEY_IS_FIRST_TIME_PROFILE)
            .remove(KeyStore.KEY_ACCESS_TOKEN)
            .remove(KeyStore.KEY_REFRESH_TOKEN)
            .apply()
        DataBaseHelper.getInstance()
            .resetTables()
    }

    fun loadFullUserFromDatabase() {
        val users = DataBaseHelper.getInstance()
            .select(DataBaseHelper.TABLE_FULL_USER, FullUser::class)
        if (users.isNotEmpty()) {
            val vkUser = VkUser.fromFullUser(
                users[FullUser.ME_ID.toInt()]
            )
            viewState.setUserFields(vkUser)
            viewState.restoreExtra()
        }
    }

    private fun saveFullUserInDatabase(user: VkUser) {
        val dbUser = user.toFullUser()
        dbUser.id = FullUser.ME_ID
        DataBaseHelper.getInstance()
            .insert(DataBaseHelper.TABLE_FULL_USER, dbUser, FullUser::class)
    }
}