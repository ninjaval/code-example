package ru.nsu.startup.vibrotinder.mvp.profile

import com.arellomobile.mvp.MvpView
import ru.nsu.startup.vibrotinder.model.vk.VkUser

interface ProfileView : MvpView {

    fun showProgress()

    fun hideProgress()

    fun showError(message: String)

    fun setUserFields(user: VkUser)

    fun showVkSyncDialog()

    fun restoreExtra()
}