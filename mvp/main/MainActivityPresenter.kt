package ru.nsu.startup.vibrotinder.mvp.main

import com.arellomobile.mvp.InjectViewState
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter

@InjectViewState
class MainActivityPresenter : BaseMvpPresenter<MainActivityView>()