package ru.nsu.startup.vibrotinder.mvp

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.slf4j.LoggerFactory

abstract class BaseMvpPresenter<T : MvpView> : MvpPresenter<T>() {

    val logger = LoggerFactory.getLogger(this.javaClass.simpleName)

    private var compositeDisposable: CompositeDisposable? = null

    fun regSubs(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable?.add(disposable)
    }

    fun clearSubs() {
        if (compositeDisposable != null) {
            compositeDisposable?.dispose()
            compositeDisposable?.clear()
        }
    }

    override fun onDestroy() {
        clearSubs()
        super.onDestroy()
    }
}