package ru.nsu.startup.vibrotinder.mvp.auth

import com.arellomobile.mvp.InjectViewState
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter

@InjectViewState
class AuthPresenter : BaseMvpPresenter<AuthView>()