package ru.nsu.startup.vibrotinder.mvp.register

import android.annotation.SuppressLint
import com.arellomobile.mvp.InjectViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.activites.RegistrationActivity
import ru.nsu.startup.vibrotinder.api.json.request.IsUserExistsRequest
import ru.nsu.startup.vibrotinder.api.json.request.SignInRequest
import ru.nsu.startup.vibrotinder.api.services.RxCommand
import ru.nsu.startup.vibrotinder.model.User
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter
import ru.nsu.startup.vibrotinder.utils.ResUtils

@InjectViewState
class RegisterPresenter : BaseMvpPresenter<RegisterView>() {

    @SuppressLint("CheckResult")
    fun requestVkUser(activity: RegistrationActivity) {
        viewState.showProgress()
        activity.getVkUserSingle()
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewState.hideProgress()
                viewState.setUserFields(it)
            }, {
                viewState.hideProgress()
                viewState.showError(it.localizedMessage)
            })
    }

    fun register(user: User, password: String) {
        viewState.showProgress()
        checkIsUserExists(user, password)
    }

    private fun checkIsUserExists(user: User, password: String) {
        logger.debug("checkIsUserExists")
        regSubs(RxCommand.makeSingleApiRequest(
            request = IsUserExistsRequest(user.email),
            function = { apiRequester, isUserExistsRequest ->
                apiRequester.checkUserExists(isUserExistsRequest)
            },
            onSuccess = {
                logger.info("onSuccess: checkIsUserExists = ${it.isExist}")
                if (it.isExist) {
                    viewState.showError(ResUtils.getStringById(R.string.error_email_exists))
                } else {
                    signUp(user, password)
                }
            },
            onError = {
                viewState.showError(it.msg)
            }
        ))
    }

    private fun signUp(user: User, password: String) {
        logger.debug("signUp")
        regSubs(RxCommand.makeSingleApiRequest(
            request = user.toSignUpRequest(password),
            function = { apiRequester, signUpRequest ->
                apiRequester.signUp(signUpRequest)
            },
            onSuccess = {
                logger.info("onSuccess: signUp")
                signIn(user, password)
            },
            onError = {
                viewState.showError(it.msg)
            }
        ))
    }

    private fun signIn(user: User, password: String) {
        logger.info("signIn")
        regSubs(RxCommand.makeSingleApiRequest(
            request = SignInRequest(user.email, password),
            function = { apiRequester, signInRequest ->
                apiRequester.signIn(signInRequest)
            },
            onComplete = {
                viewState.hideProgress()
            },
            onSuccess = {
                logger.info("onSuccess: signIn")
                viewState.navigateToMainScreen()
            },
            onError = {
                viewState.showError(it.msg)
            }
        ))
    }
}