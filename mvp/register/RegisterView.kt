package ru.nsu.startup.vibrotinder.mvp.register

import com.arellomobile.mvp.MvpView
import ru.nsu.startup.vibrotinder.model.User

interface RegisterView : MvpView {

    fun showError(message: String)

    fun setUserFields(user: User?)

    fun showProgress()

    fun hideProgress()

    fun navigateToMainScreen()
}