package ru.nsu.startup.vibrotinder.mvp.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.arellomobile.mvp.presenter.InjectPresenter
import com.google.android.material.textfield.TextInputLayout
import ru.nsu.startup.vibrotinder.R
import ru.nsu.startup.vibrotinder.activites.MainActivity
import ru.nsu.startup.vibrotinder.activites.RegistrationActivity
import ru.nsu.startup.vibrotinder.dataBase.DataBaseHelper
import ru.nsu.startup.vibrotinder.databinding.FragmentRegisterBinding
import ru.nsu.startup.vibrotinder.model.User
import ru.nsu.startup.vibrotinder.mvp.BaseValidationFragment
import ru.nsu.startup.vibrotinder.utils.AlertDialogsUtil
import ru.nsu.startup.vibrotinder.utils.RouterUtil
import ru.nsu.startup.vibrotinder.utils.Sex
import ru.nsu.startup.vibrotinder.utils.TimeUtil
import ru.nsu.startup.vibrotinder.utils.WindowUtil
import ru.nsu.startup.vibrotinder.validators.BirthdayValidator
import ru.nsu.startup.vibrotinder.validators.EmailValidator
import ru.nsu.startup.vibrotinder.validators.LengthValidator
import ru.nsu.startup.vibrotinder.validators.NumberValidator
import ru.nsu.startup.vibrotinder.validators.PasswordValidator

class RegisterFragment : BaseValidationFragment(), RegisterView {

    @InjectPresenter
    lateinit var presenter: RegisterPresenter
    private lateinit var binding: FragmentRegisterBinding

    override var titleRes = R.string.register_title

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_register, container, false)
        initUI()
        presenter.requestVkUser(activity as RegistrationActivity)
        return binding.root
    }

    private fun initUI() {
        addValidators(
            binding.register, arrayListOf(
                LengthValidator(binding.name),
                LengthValidator(binding.surname),
                EmailValidator(binding.email),
                BirthdayValidator(binding.birthday),
                NumberValidator(binding.height, 80, 240),
                NumberValidator(binding.weight, 30, 250),
                PasswordValidator(binding.password)
            )
        )

        binding.birthday.editText?.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                AlertDialogsUtil.showDatePickerDialog(
                    activity,
                    this,
                    {
                        binding.birthday.editText?.setText(it)
                    }
                )
            }
        }

        binding.register.setOnClickListener {
            presenter.register(makeUser(), binding.password.field())
        }

        // TODO REMOVE THIS
        binding.skipRegistration.setOnClickListener {
            val user = makeUser()
            DataBaseHelper.getInstance()
                .insert(DataBaseHelper.TABLE_USER, user, User::class)
            navigateToMainScreen()
        }
    }

    private fun makeUser(): User {
        return User(
            binding.name.field(),
            binding.surname.field(),
            binding.email.field(),
            getSex(),
            binding.birthday.field(),
            binding.height.field().toLong(),
            binding.weight.field().toLong(),
            TimeUtil.getZoneOffset()
        )
    }

    private fun getSex(): Long {
        return when (binding.sexMale.isChecked) {
            true -> Sex.MALE.value
            false -> Sex.FEMALE.value
        }
    }

    private fun TextInputLayout.field(): String {
        return this.editText?.editableText.toString()
    }

    override fun setUserFields(user: User?) {
        binding.name.editText?.setText(user?.name.orEmpty())
        binding.surname.editText?.setText(user?.surname.orEmpty())
        binding.email.editText?.setText(user?.email.orEmpty())
        binding.birthday.editText?.setText(TimeUtil.reformBirthday(user?.birthday.orEmpty()))

        when (Sex.fromCode(user?.sex)) {
            Sex.MALE -> binding.radioGroup.check(binding.sexMale.id)
            Sex.FEMALE -> binding.radioGroup.check(binding.sexFemale.id)
            else -> logger.debug("Unknown sex")
        }
    }

    override fun navigateToMainScreen() {
        RouterUtil.startActivityNoHistory(activity, MainActivity::class.java)
    }

    override fun showError(message: String) {
        hideProgress()
        AlertDialogsUtil.showOneBtnDialog(activity, R.string.error_title, message)
    }

    override fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
        WindowUtil.disableUserInterface(activity)
    }

    override fun hideProgress() {
        binding.progressBar.visibility = View.GONE
        WindowUtil.enableUserInterface(activity)
    }
}
