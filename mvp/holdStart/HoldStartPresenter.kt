package ru.nsu.startup.vibrotinder.mvp.holdStart

import com.arellomobile.mvp.InjectViewState
import ru.nsu.startup.vibrotinder.ProjectApplication
import ru.nsu.startup.vibrotinder.mvp.BaseMvpPresenter
import ru.nsu.startup.vibrotinder.utils.KeyStore

@InjectViewState
class HoldStartPresenter : BaseMvpPresenter<HoldStartView>() {

    fun coldStart() {
        if (isSignedIn()) {
            viewState.navigateToMainScreen()
        }
    }

    private fun isSignedIn(): Boolean {
        return ProjectApplication
            .getPreferences()
            .getBoolean(KeyStore.KEY_IS_SIGNED_IN, false)
    }
}
