package ru.nsu.startup.vibrotinder.mvp.holdStart

import com.arellomobile.mvp.MvpView

interface HoldStartView : MvpView {

    fun navigateToMainScreen()

    fun showError(message: String)
}